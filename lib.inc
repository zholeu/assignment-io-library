%define SYS_EXIT 60
%define SYS_WRITE 1
%define stdin 0
%define stdout 1

section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall                         

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                    
.loop:
    cmp byte [rdi+rax], 0          
    je .end
    inc rax                        
    jmp .loop
.end:
    ret                             

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length              
    pop rsi
    mov rdx, rax      
    mov rax, SYS_WRITE
    mov rdi, stdout         
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi  
    mov rax, SYS_WRITE
    mov rdx, 1                      
    mov rdi, stdout                 
    mov rsi, rsp               
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    mov r8, rdi
    cmp r8, 0
    jns print_uint                              
    mov rdi, '-'   
    call print_char                 
    mov rdi, r8
    neg rdi 

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    dec rsp                        
    mov rax, rdi
    mov [rsp], byte 0
    mov rbx, 10
.loop:
    xor rdx, rdx
    div rbx
    add dl, '0'
    dec rsp
    mov [rsp], dl
    test rax, rax
    jne .loop
    mov rdi, rsp
    push rcx
    call print_string
    pop rcx
    mov rdi, rsp
    call string_length
    add rsp, rax
    inc rsp    
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    mov al, byte [rdi + rcx]
    cmp al, byte [rsi + rcx]  
    jne .different
    inc rcx                             
    test al, al
    jnz .loop
    mov rax, 1
    ret
.different:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rdi, stdin
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13 
    push r12
    xor r12, r12 
    xor r13, r13
    mov r13, rdi
.space:
    call read_char 
    cmp rax, 0     
    je .success      
    cmp rax, 0x20
    je .space
    cmp rax, 0x9
    je .space
    cmp rax, 0xA
    je .space
.word:
    cmp rsi, r12
    je .overbuf
    mov byte[r13 + r12], al
    inc r12
    call read_char
    cmp rax, 0
    je .success
    cmp rax, 0x9
    je .success
    cmp rax, 0x20
    je .success
    cmp rax, 0xA
    je .success
    jmp .word
.success:
    xor rax, rax
    mov byte[r13 + r12], al
    mov rax, r13
    mov rdx, r12
    jmp .end
.overbuf:
    xor rax, rax
.end:
    pop r12 
    pop r13
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor r9, r9
    mov r8, 10
.loop:
    mov sil, byte[rdi+r9]
    test sil, sil
    jz .end
    cmp sil, '0'
    jb .end
    cmp sil, '9'
    ja .end
    sub sil, '0'
    inc r9
    mul r8
    add rax, rsi
    jmp .loop
.end:
    mov rdx, r9
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось 
parse_int:
    xor rax, rax
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .signed
    call parse_uint
    ret
.signed:
    inc rdi
    call parse_uint
    test rdx, rdx
    jnz .end
    ret 
.end:
    inc rdx
    neg rax
    ret
    
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; Input: 
;     rdi - указатель на строку
;     rsi - указатель на буфер
;     rdx - длина буфера
string_copy:
    mov r8, rsi
    mov rsi, rdi
    mov rdi, r8
.loop:
    cmp rdx, rax
    je .overbuf
    movsb
    inc rax
    jmp .loop  
.overbuf:
    xor rax, rax                        
    ret
